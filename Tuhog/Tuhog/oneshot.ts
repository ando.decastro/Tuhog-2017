﻿module OneShot {

    export var playerName = 'anonymoo';

    export class Game {
        game: Phaser.Game;
        constructor() {
            //475, 845
            //607, 1024
            this.game = new Phaser.Game(475, 845, Phaser.AUTO, 'gamediv', { preload: this.preload, create: this.create});

            this.game.state.add('TitleScreenState', TitleScreenState, false);
            this.game.state.add('PlayGameState', PlayGameState, false);

            this.game.state.start('TitleScreenState', true, true);
        }
        preload() {
        }
        create() {
        }        
    }
    
    enum AminalState {
        none = 0,
        movingLeft,
        movingRight,
        onHold,
        onHit,
        missed
    }

    export class PlayGameState extends Phaser.State {
        
        AMINAL_SIZE = 100;
        AMINAL_SPACING = 2;
        AMINAL_SIZE_SPACED = this.AMINAL_SIZE + this.AMINAL_SPACING;

        AMINAL_HIT_SCORE = 100;
        AMINAL_ACCURACY_SCORE = 100;
        AMINAL_SCALE = 1;// 0.75;
        AMINAL_COUNT = 5;

        game: Phaser.Game;
        aminals: Phaser.Group;
        tweens: Phaser.Group;

        bound = { left: (this.AMINAL_SIZE * 0.5), right: 475 - (this.AMINAL_SIZE * 0.5) };
        
        onHold: boolean = false;
        timer: number = 0;
        startHoldY: number = 0;
        
        arrow: Phaser.Sprite;

        shootSfx;
        pigSfx;
        chickenSfx;
        pigmissedSfx;
        cowSfx;
        cowmissedSfx;
        splatSfx;
        splatmissedSfx;
        hitSfx;
        btnSfx;
        scoreBanner: Phaser.Sprite;
        window_box: Phaser.Sprite;
        resetBtn: Phaser.Sprite;
        lastScore;

        preload() {
            this.game.load.spritesheet("aminals", "assets/tuhog_aminals.png", this.AMINAL_SIZE, this.AMINAL_SIZE);

            this.game.load.audio('pigSfx', "assets/Pigpissd.mp3");
            this.game.load.audio('pigmissedSfx', "assets/pigfarm.mp3");
            this.game.load.audio('cowSfx', "assets/cow-moo1.wav");
            this.game.load.audio('cowmissedSfx', "assets/cowMissed.mp3");
            this.game.load.audio('splatSfx', "assets/splatHit.mp3");
            this.game.load.audio('splatmissedSfx', "assets/splatmissed.mp3");
            this.game.load.audio('shootSfx', "assets/shoot.mp3");
            this.game.load.audio('chickenSfx', "assets/chicken.mp3");

            this.game.load.image('scoreBanner', 'assets/score_overlay.png');
            this.game.load.image('window_box', 'assets/window_box.png');
            this.game.load.bitmapFont('Supersonic', 'assets/fonts/Supersonic Rocketship.png', 'assets/fonts/Supersonic Rocketship.xml');
            this.game.load.bitmapFont('Supersonic_w', 'assets/fonts/Supersonic Rocketship_w.png', 'assets/fonts/Supersonic Rocketship_w.xml');
            
            this.game.load.spritesheet("resetBtn", "assets/replay_button.png", 113, 118);
        }

        create() {
            this.game.physics.startSystem(Phaser.Physics.ARCADE);
            this.game.stage.backgroundColor = "#CE9333";
            var starfield = this.game.add.sprite(0, 0, 'bg');//this.game.add.tileSprite(0, 0, 800, 600, 'bg');

            this.createArrow();

            this.aminals = this.game.add.group();
            this.aminals.enableBody = true;
            this.aminals.physicsBodyType = Phaser.Physics.ARCADE;
            this.createAminals();

            this.pigSfx = this.game.add.audio('pigSfx');
            this.pigmissedSfx = this.game.add.audio('pigmissedSfx');
            this.cowSfx = this.game.add.audio('cowSfx');
            this.cowmissedSfx = this.game.add.audio('cowmissedSfx');
            this.splatSfx = this.game.add.audio('splatSfx');
            this.splatmissedSfx = this.game.add.audio('splatmissedSfx');
            this.shootSfx = this.game.add.audio('shootSfx');
            this.hitSfx = this.game.add.audio('hitSfx');
            this.chickenSfx = this.game.add.audio('chickenSfx');
            this.btnSfx = this.game.add.audio('btnSfx');

            var fire = this.game.add.sprite(this.game.world.width * 0.3, this.game.world.height * 0.98, 'fire');
            fire.anchor.setTo(0.5, 1);
            var burn = fire.animations.add('burn');
            fire.animations.play('burn', 14, true);

            this.game.input.onDown.add(this.onDown, this);
            this.game.input.onUp.add(this.onUp, this);


            this.window_box = this.game.add.sprite(this.game.world.centerX, this.game.world.height * 1.5, 'window_box');
            this.window_box.anchor.setTo(0.5, 0.5);


            this.resetBtn = this.game.add.sprite(this.game.world.centerX, this.game.world.height * 1.5, 'resetBtn');
            this.resetBtn.anchor.setTo(0.5, 0.5);


            this.resetBtn.inputEnabled = true;
            var that = this;
            this.resetBtn.events.onInputDown.add(function (resetBtn) {
                resetBtn.frame = 1;
                that.btnSfx.play();
            });
            this.resetBtn.events.onInputUp.add(function (resetBtn) {
                resetBtn.frame = 0;
                that.resetGame();
            });


            this.scoreBanner = this.game.add.sprite(this.game.world.centerX, this.game.world.height * -0.5, 'scoreBanner');
            this.scoreBanner.anchor.setTo(0.5, 0.5);

            var ranking = this.game.make.bitmapText(0, 5, 'Supersonic_w', 'RANKING', 40);
            ranking.anchor.set(0.5);
            ranking.align = 'center';
            this.scoreBanner.addChild(ranking);

           
            /*
            this.scoreBanner.data.text = this.game.make.text(0, 25, '100.000');
            this.scoreBanner.data.text.anchor.set(0.5);
            this.scoreBanner.data.text.align = 'center';

            this.scoreBanner.data.text.font = 'Comic Sans MS';
            this.scoreBanner.data.text.fontSize = 50;
            this.scoreBanner.data.text.fontWeight = 'bold';
            this.scoreBanner.data.text.fill = '#ffffff';
            */

            /*
            this.window_box.data.textScore = this.game.make.bitmapText(0, 10, 'Supersonic_w', OneShot.playerName, 50);
            this.window_box.data.textScore.anchor.set(0.5);
            this.window_box.data.textScoret.align = 'center';
            this.window_box.addChild(this.window_box.data.textScore);

            */

            this.window_box.data.text = this.game.make.bitmapText(0, this.window_box.centerX * -0.65, 'Supersonic', 'xxxx - xx.xxx', 20);
            this.window_box.data.text.anchor.setTo(0.5,0);
            this.window_box.data.text.align = 'center';
            this.window_box.addChild(this.window_box.data.text);

            this.lastScore = this.game.make.bitmapText(0, this.window_box.centerX * -0.95, 'Supersonic', 'xx.xxx', 50);
            this.lastScore.anchor.setTo(0.5, 0);
            this.lastScore.align = 'center';
            this.window_box.addChild(this.lastScore);

            /*
            this.window_box.data.text = this.game.make.text(0, 25, '100.000');
            this.window_box.data.text.anchor.set(0.5);
            this.window_box.data.text.align = 'center';
            this.window_box.data.text.font = 'Comic Sans MS';
            this.window_box.data.text.fontSize = 50;
            this.window_box.data.text.fontWeight = 'bold';
            this.window_box.data.text.fill = '#ffffff';

            this.window_box.addChild(this.window_box.data.text);
            */
        }

        createArrow() {
            this.arrow = this.game.add.sprite(this.game.world.centerX, this.game.world.height * 0.8, "arrow");
            this.arrow.anchor.setTo(0.5, 0);
            this.arrow.name = 'arrow';
            this.game.physics.enable(this.arrow, Phaser.Physics.ARCADE);
            this.arrow.data.didShot = false;
            this.arrow.data.onShot = false;
            this.arrow.data.speed = 100;
        }

        createAminals() {
            for (var i = 0; i < this.AMINAL_COUNT; i++) {
                var aminal = this.aminals.create(this.game.rnd.between(this.bound.left, this.bound.right), 50 + (i * this.AMINAL_SIZE_SPACED * this.AMINAL_SCALE), "aminals");
                aminal.name = 'aminal :' + i.toString();
                aminal.scale.setTo(this.AMINAL_SCALE, this.AMINAL_SCALE);
                aminal.inputEnabled = true;
                aminal.events.onInputDown.add(function (aminal) {
                    if (aminal.name == 'aminal :0') { //-- chicken
                        this.chickenSfx.play();
                    } else if (aminal.name == 'aminal :1') { //-- beef
                        this.cowSfx.play();
                    } else if (aminal.name == 'aminal :2') { //-- pork
                        this.pigSfx.play();
                    } else if (aminal.name == 'aminal :3') { //-- tomato
                        this.splatSfx.play();
                    } else if (aminal.name == 'aminal :4') { //-- onion
                        this.splatSfx.play();
                    }
                    aminal.data.state = AminalState.onHold;
                }, this);
                aminal.events.onInputUp.add(function (aminal) {
                    aminal.data.state = !!(this.game.rnd.frac() > 0.5) ? AminalState.movingLeft : AminalState.movingRight;
                }, this);

                //-- add physics details
                aminal.data.state = AminalState.missed;
                this.game.physics.enable(aminal, Phaser.Physics.ARCADE);
                aminal.body.moves = false;
                aminal.body.gravity.set(0, 500);
                aminal.body.bounce.set(0.2);
                aminal.body.collideWorldBounds = true;

                aminal.frame = i;
                aminal.anchor.setTo(0.5, 0.5);
                aminal.data.index = i;
                aminal.data.score = 0;
                aminal.data.state = !!(this.game.rnd.frac() > 0.5) ? AminalState.movingLeft : AminalState.movingRight;
            }
        }

        resetAminals() {

            this.aminals.forEach(function (aminal: Phaser.Sprite) {
                aminal.position.setTo(this.game.rnd.between(this.bound.left, this.bound.right), 50 + (aminal.data.index * this.AMINAL_SIZE_SPACED * this.AMINAL_SCALE));
                aminal.data.state = !!(this.game.rnd.frac() > 0.5) ? AminalState.movingLeft : AminalState.movingRight;
                aminal.body.moves = false;
                aminal.data.score = 0;
                aminal.frame = aminal.data.index;


            }, this);
        }

        resetGame() {
            console.log('resetGame');
            this.scoreBanner.data.tween.stop();
            this.window_box.data.tween.stop();
            this.resetBtn.data.tween.stop();
            this.arrow.data.didShot = false;
            this.arrow.data.onShot = false;
            this.arrow.y = this.game.world.height * 0.8;
            this.scoreBanner.y = this.game.world.height * -0.5;
            this.window_box.y = this.game.world.height * 1.5;
            this.resetBtn.y = this.game.world.height * 1.5;
            this.resetAminals();
        }

        //-- inputs
        onDown() {
            console.log("on DOWN");
            if (this.arrow.data.didShot) {
                //this.resetGame();
                return;
            }

            this.onHold = true;
            this.timer = 0;
            this.startHoldY = this.input.position.y;
        }

        onUp() {

            
            if (!this.onHold)
                return;
            this.onHold = false;
            var distance = this.startHoldY - this.input.position.y;

            //console.log(distance);
            if (distance < 100) // swipe down or little swipe
                return;

            var speed = distance / this.timer;
            this.arrow.data.onShot = true;
            this.arrow.data.speed = speed;
            //console.log(this.timer);
            console.log("SHOOT!");
            this.timer = 0;
            this.shootSfx.play();
            var spearfx = this.game.add.sprite(this.game.world.centerX, this.game.world.height * 0.99, 'spearFX');
            spearfx.anchor.setTo(0.5, 1);
            spearfx.animations.add('poof', [0, 1, 2, 3], 14);
            spearfx.animations.play('poof');
            spearfx.animations.currentAnim.onComplete.add(function () {
                spearfx.destroy(true);
            });
        }
        
        update() {
            var deltaTime = this.game.time.elapsed / 1000;
            this.aminals.forEach(function (aminal) {

                if (aminal.data.state == AminalState.onHit) {
                    //aminal.rotation += this.game.rnd.between(-Math.PI, Math.PI) * deltaTime * 5;
                }
                else if (aminal.data.state == AminalState.movingLeft) {
                    aminal.x += deltaTime * 100;
                    aminal.rotation += Math.PI * deltaTime;
                    if (aminal.x >= this.bound.right) {
                        aminal.x = this.bound.right;
                        aminal.data.state = AminalState.movingRight;
                    }
                }
                else if (aminal.data.state == AminalState.movingRight) {
                    aminal.x -= deltaTime * 100;
                    aminal.rotation -= Math.PI * deltaTime;
                    if (aminal.x <= this.bound.left) {
                        aminal.x = this.bound.left;
                        aminal.data.state = AminalState.movingLeft;
                    }
                }
                else if (aminal.data.state == AminalState.onHold) {
                    aminal.rotation += this.game.rnd.between(-Math.PI, Math.PI) * deltaTime * 5;
                }
            }, this);
            

            if (this.onHold) {
                this.timer += deltaTime;
            }

            if (this.arrow.data.onShot) {
                var inDeep = -30;
                this.arrow.y -= this.arrow.data.speed * deltaTime;
                this.game.physics.arcade.overlap(this.arrow, this.aminals, this.collisionHandler, null, this);
                if (this.arrow.y <= inDeep) {

                    this.arrow.y = inDeep;

                    var spearfx = this.game.add.sprite(this.game.world.centerX, 0, 'spearFX');
                    spearfx.anchor.setTo(0.5, 0);
                    spearfx.animations.add('poof', [4], 12);
                    spearfx.animations.play('poof');
                    spearfx.animations.currentAnim.onComplete.add(function () {
                        spearfx.destroy(true);
                    });

                    this.hitSfx.play();

                    this.arrow.data.didShot = true;
                    this.arrow.data.onShot = false;

                    var score: number = 0;
                    this.aminals.forEach(function (aminal) {
                        score += aminal.data.score;
                    }, this);
                    score = (score / (this.AMINAL_COUNT * (this.AMINAL_HIT_SCORE + this.AMINAL_ACCURACY_SCORE))) * 100;
                    console.log('MY SCORE:' + score);

                    this.lastScore.setText(Phaser.Math.roundTo(score, -3));

                    //this.window_box.data.textScore.setText(Phaser.Math.roundTo( score,-3) );

                    this.scoreBanner.data.tween = this.game.add.tween(this.scoreBanner).to({ y: this.game.world.height * 0.15 }, 2000, Phaser.Easing.Back.Out, true);

                    this.window_box.data.text.setText('');
                    this.window_box.data.tween = this.game.add.tween(this.window_box).to({ y: this.game.world.centerY * 1.1 }, 500, Phaser.Easing.Linear.None, true, 200);


                    /*
                    var that = this;
                    leaderboard.getLeaderboardString(function (data) {

                        console.log('receiveData');
                        //that.window_box.data.text.setText(data);
                    });
                    */
                   

                    var that = this;
                    leaderboard.sendScore(playerName, score, function () {
                        //-- call when success send score
                        leaderboard.getLeaderboardString(function (data) {
                            //-- call when finish fetching score
                            console.log('receiveData');
                            console.log(data);

                            that.resetBtn.data.tween = that.game.add.tween(that.resetBtn).to({ y: that.game.world.height * 0.9 }, 500, Phaser.Easing.Linear.None, true);

                            that.window_box.data.text.setText(data);


                        });
                    });
                    //this.scoreBanner.data.tween.play();
                   // this.scoreBanner
                }
            }
        }


        collisionHandler(arrow: Phaser.Sprite, aminal: Phaser.Sprite) {
            if (aminal.data.state == AminalState.missed || aminal.data.state == AminalState.onHit)
                return;
            /*
            console.log(item.name);
            console.log(item2.name);
            */
            var xDif: number = 0;
            if (arrow.x < aminal.x)
                xDif = aminal.x - arrow.x;
            else 
                xDif = arrow.x - aminal.x;
            
            //console.log(xDif);
             
            if (xDif > 30) { //*** MISSED AMINAL!!! ***
                aminal.data.state = AminalState.missed;
                aminal.body.moves = true;
                aminal.body.velocity.x = arrow.x < aminal.x ? 300 : -300;
                aminal.body.velocity.y = -300;
                aminal.data.score = 0;
                
                /*
                
                if (aminal.name == 'aminal :0') { //-- chicken
                } else if (aminal.name == 'aminal :1') { //-- beef
                    this.cowSfx.play();
                } else if (aminal.name == 'aminal :2') { //-- pork
                    this.pigSfx.play();
                } else if (aminal.name == 'aminal :3') { //-- tomato
                    this.splatSfx.play();
                } else if (aminal.name == 'aminal :4') { //-- onion
                    this.splatSfx.play();
                }

                */
                //this.splatmissedSfx.play();
                

            }
            else { //*** HIT AMINAL!!! ***

                aminal.data.state = AminalState.onHit;
                var accuracy = xDif / 30;
                aminal.data.score = this.AMINAL_HIT_SCORE + (this.AMINAL_ACCURACY_SCORE * accuracy);


                if (aminal.name == 'aminal :0') { //-- chicken
                    aminal.frame = 5;
                } else if (aminal.name == 'aminal :1') { //-- beef
                    aminal.frame = 6;
                } else if (aminal.name == 'aminal :2') { //-- pork
                    aminal.frame = 7;
                } else if (aminal.name == 'aminal :3') { //-- tomato
                } else if (aminal.name == 'aminal :4') { //-- onion
                }


                /*
                console.log(aminal.name);

                if (aminal.name == 'aminal :4')
                    this.pigSfx.play();
                else if (aminal.name == 'aminal :3')
                    this.cowSfx.play();
                else if (aminal.name == 'aminal :0' || aminal.name == 'aminal :2')
                    this.splatSfx.play();
                */


                this.splatSfx.play();
            }
            

            
        }

        render() {

        }


    }

}

