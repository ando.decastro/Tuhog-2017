var LeaderBoard = (function () {
    function LeaderBoard() {
        this.leaderboardData = {};
        this.turnOn = true;
        if (!this.turnOn)
            return;
        // Initialize Firebase
        var config = {
            apiKey: "AIzaSyDk0dRoQFbPKolcxYTN92sekjla47Y6PkI",
            authDomain: "tuhog2017.firebaseapp.com",
            databaseURL: "https://tuhog2017.firebaseio.com",
            projectId: "tuhog2017",
            storageBucket: "tuhog2017.appspot.com",
            messagingSenderId: "702440971053"
        };
        firebase.initializeApp(config);
        this.messaging = firebase.messaging();
        firebase.auth().signInAnonymously().catch(function (error) {
            // Handle Errors here.
            var errorCode = error.code;
            var errorMessage = error.message;
            // ...
        });
    }
    LeaderBoard.prototype.fetchData = function (callback) {
        if (callback === void 0) { callback = null; }
        if (!this.turnOn)
            return;
        var query = firebase.database().ref("leaderboard").orderByKey();
        var that = this;
        query.once("value").then(function (snapshot) {
            snapshot.forEach(function (childSnapShot) {
                var key = childSnapShot.key;
                var childData = childSnapShot.val();
                //console.log(key);
                //console.log(childData);
                that.leaderboardData[key] = {};
                that.leaderboardData[key].score = childData.score;
                that.leaderboardData[key].name = childData.name;
            });
            console.log(that.leaderboardData);
            if (typeof callback === "function") {
                callback(that.leaderboardData);
            }
        });
    };
    LeaderBoard.prototype.getLeaderboardString = function (p_callback) {
        if (p_callback === void 0) { p_callback = null; }
        this.fetchData(function (p_data) {
            var listText = '';
            var list = {};
            for (var _i = 0, _a = Object.keys(p_data); _i < _a.length; _i++) {
                var key = _a[_i];
                list[p_data[key].name] = p_data[key].score;
            }
            //console.log(list);
            var keysSorted = Object.keys(list).sort(function (a, b) { return list[a] - list[b]; });
            var topCount = 10;
            for (var i = keysSorted.length - 1; i >= (keysSorted.length < topCount ? 0 : keysSorted.length - topCount); i--) {
                listText = listText + keysSorted[i] + ' - ' + Phaser.Math.roundTo(parseFloat(list[keysSorted[i]]), -3) + '\n\n';
            }
            if (typeof p_callback === "function") {
                p_callback(listText);
            }
        });
    };
    LeaderBoard.prototype.init = function () {
        if (!this.turnOn)
            return;
        console.log('init leaderboard');
        this.fetchData();
        //this.sendScore("doandz", 300);
    };
    LeaderBoard.prototype.sendScore = function (pName, pScore, pCallback) {
        if (pCallback === void 0) { pCallback = null; }
        if (!this.turnOn)
            return;
        console.log('sendScore - ' + pName + ' : ' + pScore);
        this.fetchData(function (p_leaderBoardData) {
            var scoreSent = false;
            if (Object.keys(p_leaderBoardData).length > 0) {
                for (var key in p_leaderBoardData) {
                    if (p_leaderBoardData[key].name == pName) {
                        if (pScore > p_leaderBoardData[key].score) {
                            var db = firebase.database();
                            db.ref("leaderboard/" + key + "/score").set(pScore); //-- update score on db
                        }
                        scoreSent = true;
                    }
                }
            }
            if (!scoreSent) {
                var rootRef = firebase.database().ref();
                var leaderboardRef = rootRef.child("leaderboard");
                leaderboardRef.push({
                    name: pName,
                    score: pScore
                });
            }
            if (typeof pCallback === "function") {
                pCallback();
            }
        });
        /*
        if (Object.keys(this.leaderboardData).length > 0) {

            for (var key in this.leaderboardData) {
                if (this.leaderboardData[key].name == pName) {
                    if (pScore > this.leaderboardData[key].score) {
                        var db = firebase.database();
                        db.ref("leaderboard/" + key + "/score").set(pScore);
                    }

                    return;
                }
            }

        }
        
        var rootRef = firebase.database().ref();
        var leaderboardRef = rootRef.child("leaderboard");
        leaderboardRef.push({
            name: pName,
            score: pScore
        });
        this.fetchData();*/
    };
    return LeaderBoard;
}());
var leaderboard = new LeaderBoard();
//# sourceMappingURL=leaderboard.js.map